%%%%%%%%%%% In the name of GOD %%%%%%%%%%%
% Course: DSP BSC course - Dr. Sheikhzadeh
% demo program by amir hossein rassafi in Feb,2018
%%
clc
clear
n = 1:10;
a = 0.9
h = a.^n;
x = ones(1,20);
%%
figure
subplot(3,2,1)
stem(h)
ylabel('h')
subplot(3,2,2)
stem(x)
subplot(3,2,[3,4])
stem(conv(h,x))
legend('conv')
subplot(3,2,[5,6])
stem(filter(h,[1],x))

