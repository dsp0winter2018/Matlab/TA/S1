%%%%%%%%%%% In the name of GOD %%%%%%%%%%%
% Course: DSP BSC course - Dr. Sheikhzadeh
% demo program by amir hossein rassafi in Feb,2018
%%
clc
clear
%length of moving average
L = 9 
F1 = 1
F2 = 20
Fs = 100;
n = 0:1/Fs:2;
%Declare two sin wave with 1 and 20 Hz
s1 = 10*sin(2*pi*F1*n);
s2 = 20*sin(2*pi*F2*n);
input = s1+s2;
%MA freq response
h=ones(1,L);
%%
%plot output
figure('Name','Time domain');
subplot(3,2,1);
plot(s1);
legend(sprintf("%d Hz",F1))
subplot(3,2,2);
plot(s2);
legend(sprintf("%d Hz",F2))
subplot(3,2,[3,4]);
plot(input);
legend('sum')
subplot(3,2,[5,6]);
plot(conv(input,h));
legend(sprintf("conv with %d MA",L))
figure('Name','freq response of input');
freqz(input);
legend("sum DTFT")
figure;
freqz(h);
legend("MA DTFT")
figure;
freqz(conv(input,h));
legend("output DTFT")


